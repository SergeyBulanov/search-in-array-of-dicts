from qwert.lib import query, Query, Select, FieldFilter


friends = [
    {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'},
    {'name': 'Emily', 'gender': 'female', 'sport': 'volleyball'},
]

print(
    query(
        friends,
    )
)

print(
    query(
        friends,
        Select('name', 1),
    )
)


print(
    query(
        friends,
        Select('name', 'gender', 'sport'),
        FieldFilter('sport', *('Basketball', 'volleyball')),
    )
)

print(
    query(
        friends,
        Select(),
        FieldFilter('sport', *('Basketball',)) & FieldFilter('gender', *('male',))
    )
)


print(
    query(
        friends,
        Select(),
        FieldFilter('sport', *('Basketball',)) | FieldFilter('sport', *('volleyball',))
    )
)

f = FieldFilter('sport', *('Basketball',)) | FieldFilter('sport', *('volleyball',))
print(
    query(
        friends,
        Select(),
        f
    )
)

f = (FieldFilter('sport', *('Basketball',)) | FieldFilter('sport', *('volleyball',))) & FieldFilter('name', *('Sam',))
print(
    query(
        friends,
        Select(),
        f
    )
)

f = (FieldFilter('sport', *('Basketball',)) | FieldFilter('sport', *('volleyball',))) & FieldFilter('name', *('Sam',))

query_ = Query(
    friends,
    Select(),
    f,
)

for i in query_:
    print(i)
