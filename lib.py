

class Select:
    def __init__(self, *keys):
        self.keys = keys

    def __call__(self, data: dict):
        return {key: data.get(key) for key in self.keys} if len(self.keys) else data


class Filter:
    def __init__(self):
        self.operator = None
        self.other = lambda x: True

    def __and__(self, other):
        self.operator = 'and'
        self.other = other
        return self

    def __or__(self, other):
        self.operator = 'or'
        self.other = other
        return self

    def useFilter(self, data):
        return True

    def __call__(self, data: dict):
        s = {
            'or': lambda x: self.useFilter(x) or self.other(x),
            'and': lambda x: self.useFilter(x) and self.other(x),
            None: lambda x: self.useFilter(x)
        }
        return s[self.operator](data)


class FieldFilter(Filter):
    def __init__(self, key, *values):
        super().__init__()

        self.key = key
        self.values = values

    def useFilter(self, data):
        return data.get(self.key) in self.values


def query(*args):
    q = Query(*args)
    return q.getAll()


class Query:
    def __init__(self, data: list, select: Select = Select(), filter: Filter = Filter()):
        self.data = data
        self.dataId = 0

        self.select = select

        self.filter = filter

    def __iter__(self):
        return self

    def __next__(self) -> dict:
        while True:
            try:
                data_ = self.data[self.dataId]
                self.dataId += 1

                assert type(data_) == dict, ImportError(f'data[{self.dataId}] != dict')

                if self.filter(data_):
                    return self.select(data_)

            except IndexError:
                raise StopIteration

    def getAll(self):
        return list(self)
